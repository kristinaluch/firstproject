package main.java.com.datastorage.utils;

import main.java.com.datastorage.models.Person;
import main.java.com.datastorage.service.ValidationService;

import java.io.*;
import java.util.Scanner;

public class CreatePerson{

    private static final String MSG_ENTER_FNAME = "Enter first name: ";
    private static final String MSG_ENTER_LNAME = "Enter last name: ";
    private static final String MSG_ENTER_AGE = "Enter age: ";
    private static final String MSG_ENTER_CITY = "Enter city: ";
    private static final String PATH_ID = "src/PersonID.txt";

    Scanner scanner = new Scanner(System.in);
    Person person = new Person();


    public Person createPerson(){


        String idCounter = "";

        File myObj = new File(PATH_ID);
        if(myObj.length() == 0){
            idCounter += "1";
        } else {
            try (BufferedReader reader = new BufferedReader(new FileReader(PATH_ID))) {
                String line;
                while ((line = reader.readLine()) != null) {
                    idCounter += line;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            myObj = new File(PATH_ID);
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        person.setId(Integer.parseInt(idCounter));
        System.out.println(MSG_ENTER_FNAME);
        person.setFname(new ValidationService(scanner).validateString());
        System.out.println(MSG_ENTER_LNAME);
        person.setLname(new ValidationService(scanner).validateString());
        System.out.println(MSG_ENTER_AGE);
        person.setAge(new ValidationService(scanner).validateAge());
        System.out.println(MSG_ENTER_CITY);
        person.setCity(new ValidationService(scanner).validateString());

        try {
            myObj = new File(PATH_ID);
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            }
            else {
                System.out.println("Generate id...");
            }
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        try {
            FileWriter myWriter = new FileWriter(PATH_ID);
            int intID = Integer.parseInt(idCounter);
                intID++;
                myWriter.write(String.valueOf(intID));
            myWriter.close();
            //System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("Can't get ID");
            e.printStackTrace();
        }
        System.out.println(person.toString());
        return person;
    }

    public Person updatePerson(int id){

        person.setId(id);
        System.out.println(MSG_ENTER_FNAME);
        person.setFname(new ValidationService(scanner).validateString());
        System.out.println(MSG_ENTER_LNAME);
        person.setLname(new ValidationService(scanner).validateString());
        System.out.println(MSG_ENTER_AGE);
        person.setAge(new ValidationService(scanner).validateAge());
        System.out.println(MSG_ENTER_CITY);
        person.setCity(new ValidationService(scanner).validateString());
        System.out.println(person.toString());
        return person;
    }
}
