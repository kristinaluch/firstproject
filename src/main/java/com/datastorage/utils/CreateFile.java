package main.java.com.datastorage.utils;

import java.io.File;
import java.io.IOException;

public class CreateFile {
    public File createFile(String PATH){
        File file = new File(PATH);
        if (!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return file;
    }
}
