package main.java.com.datastorage.service;

import main.java.com.datastorage.models.Person;

public interface IFilesFactory {

    //Person[] readRPersons();

    void create();

    void update();

    void delete();

    void read();

    void readAll();
}
