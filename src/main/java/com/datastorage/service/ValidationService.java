package main.java.com.datastorage.service;

import main.java.com.datastorage.service.impl.JSONService;
import org.w3c.dom.ls.LSOutput;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidationService {

    private static final String MSG_INVALID_STRING = "Invalid string! The string must contain only letters!";
    private static final String MSG_INVALID_NUMBER = "Invalid number! The string must contain only number!";
    private static final String MSG_INVALID_AGE = "Invalid age! The age can't be lesser then 0 and higher then 150!";

    private Scanner scanner;

    public ValidationService(Scanner scanner) {
        this.scanner = scanner;
    }

    public String validateString(){
        String str;

        while(true){
            str = scanner.nextLine();
            Pattern pattern = Pattern.compile("[0-9`~!@#$%^&*()_+=/?><,.;:\n{}\"]");
            Matcher matcher = pattern.matcher(str);
            if(matcher.find()){
                System.out.println(MSG_INVALID_STRING);
                continue;
            }
            break;
        }
        return str.trim();
    }

    public int validateAge(){
        String number;

        while(true){
            number = scanner.nextLine();
            Pattern pattern = Pattern.compile("\\D");
            Matcher matcher = pattern.matcher(number);
            if(matcher.find()){
                System.out.println(MSG_INVALID_NUMBER);
                continue;
            }
            try {
                if (Integer.parseInt(number) > 150 || Integer.parseInt(number) < 0) {
                    System.out.println(MSG_INVALID_AGE);
                    continue;
                }
            }
            catch (Exception e){
                System.out.println("Number very long.");
            }
            break;
        }
        return Integer.parseInt(number);
    }

    public int validateID(){
        String number;

        while(true){
            number = scanner.next();
            Pattern pattern = Pattern.compile("\\D");
            Matcher matcher = pattern.matcher(number);
            if(matcher.find()){
                System.out.println(MSG_INVALID_NUMBER);
                continue;
            }
                break;
        }
        return Integer.parseInt(number);
    }

}
