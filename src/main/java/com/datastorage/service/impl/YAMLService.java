package main.java.com.datastorage.service.impl;


import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.undercouch.bson4jackson.BsonFactory;
import main.java.com.datastorage.service.CommandService;
import main.java.com.datastorage.models.Person;
import main.java.com.datastorage.service.IFilesFactory;
import main.java.com.datastorage.utils.AppendingObjectOutputStream;
import main.java.com.datastorage.utils.CreatePerson;
import main.java.com.datastorage.service.ValidationService;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;



public class YAMLService implements IFilesFactory {

    private Scanner scanner;
    private ValidationService validator;
    private CreatePerson createPerson;
    private ObjectMapper mapper;

    public YAMLService(Scanner scanner, ValidationService validator, CreatePerson createPerson, ObjectMapper mapper) {
        this.scanner = scanner;
        this.validator = validator;
        this.createPerson = createPerson;
        this.mapper = mapper;
    }

    private static final String PATH = "src/yamlPerson.yaml";

    private static final String MSG_EMPTY_FILE = "File is empty.";
    private static final String MSG_ENTER_ID = "Enter ID...";
    private static final String MSG_ENTER_FNAME = "Enter first name...";
    private static final String MSG_ENTER_LNAME = "Enter last name...";
    private static final String MSG_ENTER_AGE = "Enter age...";
    private static final String MSG_ENTER_CITY = "Enter city...";

    @Override
    public void create() {
        Person person = new CreatePerson().createPerson();
        Person [] persons;
        Person [] tempPersons;

        if(checkSize() == 0){
            persons = new Person[1];
            persons[0] = person;
        } else {
            persons = new Person[checkSize()+1];
            tempPersons = returnPersons();
            for (int i = 0; i < persons.length - 1; i++) {
                persons[i] = tempPersons[i];
            }
            persons[persons.length - 1] = person;
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            mapper.writeValue(baos, persons);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (FileOutputStream fos = new FileOutputStream(PATH)) {
            baos.writeTo(fos);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }

    @Override
    public void read(){

        int id = validator.validateID();

        try {
            FileInputStream fis = new FileInputStream(PATH);

            ByteArrayInputStream bais = new ByteArrayInputStream(fis.readAllBytes());

            try {
                Person[] persons = mapper.readValue(bais, Person[].class);
                for (Person pp : persons) {
                    if (pp.getId() == id) {
                        System.out.println(pp);
                        return;
                    }
                }
                System.out.println("User with ID " + id + " isn't exist.");
            }
            catch (Exception e){
                System.out.println(MSG_EMPTY_FILE);
            }

        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    @Override
    public void readAll(){

        try {
            FileInputStream fis = new FileInputStream(PATH);

            ByteArrayInputStream bais = new ByteArrayInputStream(fis.readAllBytes());

            try {
                Person[] persons = mapper.readValue(bais, Person[].class);
                for (Person pp: persons) {
                    System.out.println(pp.toString());
                }
            }
            catch (Exception e){
                System.out.println(MSG_EMPTY_FILE);
            }

        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    @Override
    public void update(){
        int id = validator.validateID();
        boolean flag = true;

        try {
            FileInputStream fis = new FileInputStream(PATH);

            ByteArrayInputStream bais = new ByteArrayInputStream(fis.readAllBytes());

            try {
                Person[] persons = mapper.readValue(bais, Person[].class);
                for (Person pp : persons) {
                    if (pp.getId() == id) {
                        System.out.println(MSG_ENTER_FNAME);
                        pp.setFname(validator.validateString());
                        System.out.println(MSG_ENTER_LNAME);
                        pp.setLname(validator.validateString());
                        System.out.println(MSG_ENTER_AGE);
                        pp.setAge(validator.validateAge());
                        System.out.println(MSG_ENTER_CITY);
                        pp.setCity(validator.validateString());
                        flag = false;
                    }
                }
                if(flag){
                    System.out.println("Element with ID " + id + " doesn't exist.");
                }

                ByteArrayOutputStream baos = new ByteArrayOutputStream();


                try {
                    mapper.writeValue(baos, persons);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try (FileOutputStream fos = new FileOutputStream(PATH)) {
                    baos.writeTo(fos);
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }
            }
            catch (Exception e){
                System.out.println(MSG_EMPTY_FILE);
            }

        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    @Override
    public void delete(){
        int id = validator.validateID();

        try {
            FileInputStream fis = new FileInputStream(PATH);

            ByteArrayInputStream bais = new ByteArrayInputStream(fis.readAllBytes());

            try {
                Person[] persons = mapper.readValue(bais, Person[].class);
                Person[] newPersons = new Person[persons.length - 1];

                for (int i = 0, j = 0; i < persons.length; i++) {
                    if (persons[i].getId() != id) {
                        newPersons[j] = persons[i];
                        j++;
                    }
                }

                ByteArrayOutputStream baos = new ByteArrayOutputStream();

                try {
                    mapper.writeValue(baos, newPersons);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try (FileOutputStream fos = new FileOutputStream(PATH)) {
                    baos.writeTo(fos);
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }
            }
            catch (Exception e){
                System.out.println("Element with ID " + id + " doesn't exist.");
            }

        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }


    public int checkSize(){
        int size = 0;

        try {
            FileInputStream fis = new FileInputStream(PATH);

            ByteArrayInputStream bais = new ByteArrayInputStream(fis.readAllBytes());

            try{
                Person [] persons = mapper.readValue(bais, Person[].class);
                size = persons.length;
            } catch (Exception a){
                System.out.println();
            }

        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        return size;
    }

    public Person [] returnPersons(){
        Person [] retPersons = new Person[checkSize()];
        try {
            FileInputStream fis = new FileInputStream(PATH);

            ByteArrayInputStream bais = new ByteArrayInputStream(fis.readAllBytes());

            Person [] persons = mapper.readValue(bais, Person[].class);

            retPersons = persons;

        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        return retPersons;
    }

}

