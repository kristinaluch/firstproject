package main.java.com.datastorage.cache;

import main.java.com.datastorage.service.IFilesFactory;


public class FactoryCache {

    private static final String RESPONSE_BINARY = ".binary";
    private static final String RESPONSE_CSV = ".csv";
    private static final String RESPONSE_JSON = ".json";
    private static final String RESPONSE_XML = ".xml";
    private static final String RESPONSE_YAML = ".yaml";




    private IFilesFactory[] factories;


    public FactoryCache(IFilesFactory[] factories) {
        this.factories = factories;
    }

    public IFilesFactory getEnvironment(String factory) {

        switch (factory) {
            case RESPONSE_BINARY:
                return factories[0];
            case RESPONSE_CSV:
                return factories[1];
            case RESPONSE_JSON:
                return factories[2];
            case RESPONSE_XML:
                return factories[3];
            case RESPONSE_YAML:
                return factories[4];
            default:
                return null;
        }

    }
}
